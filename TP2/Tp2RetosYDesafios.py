
val = 5+30*20
val2 = (5+30)*20

val3 = ((4+5)*2)/5


a = 5
b = 500

c = (a+b)*2

print(val)
print(val2)
print(val3)

met = "AUG"
lis = "AAA" 
leu = "CUA"

peptido =  met + lis + lis + lis + leu + leu + met
print(peptido)

palabra = 'si'
lo_mismo = 'Si'
print(palabra == lo_mismo)

def calcular_porcentaje_CG(cadena):
    cantCG = 0
    for c in cadena:
        if(c == "C" or c == "G"):
            cantCG+=1
    return (cantCG/ len(cadena) ) * 100

cadena = "TGATAAGAGTACCCAGAATAAAATGAATAACTTTTTAAAGACAAAATCCTCTGTTATAATATTGCTAAAATTATTCAGAGTAATATTGTGGATTAAAGCCACAATAAGATTTATAATCTTAAATGATGGGACTACCATCCTTACTCTCTCCATTTCAAGGCTGACGATAAGGAGACCTGCTTTGCCGAGGAGGTACTACAGTTCTCTTCACAAACAATTGTCTTACAAAATGAATAAAACAGCACTTTGTTTTTATCTCCTGCTTTTAATATGTCCAGTATTCATTTTTGCATGTTTGGTTAGGCTAGGGCTTAGGGATTTATATATCAAAGGAGGCTTTGTACATGTGGGACAGGGATCTTATTTTAGATTTATATATCAAAGGAGGCTTTGTACATGTGGGACAGGGATCTTATTTTACAAACAATTGTCTTACAAAATGAATAAAACAGCACTTTGTTTTTATCTCCTGCTCTATTGTGCCATACTGTTGAATGTTTATAATGCATGTTCTGTTTCCAAATTTCATGAAATCAAAACATTAATTTATTTAAACATTTACTTGAAATGTTCACAAACAATTGTCTTACAAAATGAATAAAACAGCACTTTGTTTTTATCTCCTGCTTTTAATATGTCCAGTATTCATTTTTGCATGTTTGGTTAGGCTAGGGCTTAGGGATTTATATATCAAAGGAGGCTTTGTACATGTGGGACAGGGATCTTATTTTAGATTTATATATCAAAGGAGGCTTTGTACATGTGGGACAGGGATCTTATTTTACAAACAATTGTCTTACAAAATGAATAAAACAGCACTTTGTTTTTATCTCCTGCTCTATTGTGCCATACTGTTGAATGTTTATAATGCATGTTCTGTTTCCAAATTTCATGAAATCAAAACATTAATTTATTTAAACATTTACTTGAAATGTGGTGGTTTGTGATTTAGTTGATTTTATAGGCTAGTGGGAGAATTTACATTCAAATGTCTAAATCACTTAAAATTTCCCTTTATGGCCTGACAGTAACTTTTTTTTATTCATTTGGGGACAACTATGTCCGTGAGCTTCCATCCAGAGATTATAGTAGTAAATTGTAATTAAAGGATATGATGCACGTGAAATCACTTTGCAATCAT"
porcentageCG  = calcular_porcentaje_CG(cadena)
print(porcentageCG)


def aplicar_jugo_vencido(gen_normal, sorbo_a_aplicar):
    gen_mutado = gen_normal
    if(sorbo_a_aplicar == 1):
         # Primer sorbo: Cambiar todas las citosinas (C) por timinas (T)
        gen_mutado = gen_normal.replace('C', 'T')
    elif sorbo_a_aplicar == 2:
            # Segundo sorbo: Cambiar todas las adeninas (A) por guaninas (G)
            gen_mutado = gen_mutado.replace('A', 'G')
    elif sorbo_a_aplicar == 3:
            # Tercer sorbo: Cambiar todas las citosinas (C) por adeninas (A)
            gen_mutado = gen_mutado.replace('C', 'A')
    return gen_mutado

def descubrir_cantidad_de_sorbos_necesarios(gen_normal,gen_mutado_necesario):
    sorbos = 0
    while not(gen_mutado_necesario in gen_normal):
        sorbos += 1
        gen_normal = aplicar_jugo_vencido(gen_normal, sorbos)
    return sorbos


gen_normal = 'ATGGAACTTGCAATCGAAGTTGGC'
gen_mutado_necesario = 'GTTTGTGGTTG'
sorbos = descubrir_cantidad_de_sorbos_necesarios(gen_normal,gen_mutado_necesario)
print("Se necesitan {} sorbos de Jugo Vencido para transformar el gen normal en el gen mutado.".format(sorbos))


def imprimir_la_rana_con_mas_genes(gen_rana_marte, gen_rana_terrestre):
    longitud_gen_rana_marte = len(gen_rana_marte)
    longitud_gen_rana_terrestre = len(gen_rana_terrestre)

    if longitud_gen_rana_marte > longitud_gen_rana_terrestre:
        print("El gen de la proteína diminuta de la rana marciana es más largo.")
    elif longitud_gen_rana_marte < longitud_gen_rana_terrestre:
        print("El gen de la proteína diminuta de la rana terrestre es más largo.")
    else:
        print("Ambos genes tienen la misma longitud.")  

gen_rana_marte = 'ATGGAAGTTGGAATCCAAGTTGGA'
gen_rana_terrestre = 'ATGGAAGTTAATGGAAGTTGGAGGAGA'

imprimir_la_rana_con_mas_genes(gen_rana_marte,gen_rana_terrestre)

def habemus_clon_totales():
    cant_clones = 0
    for i in range(0, 20):
        if(i == 0):
            cant_clones+=2
        else: 
            cant_clones= cant_clones *  2
        print('¡Somos {} clones nuevos!'.format(cant_clones))

habemus_clon_totales()

