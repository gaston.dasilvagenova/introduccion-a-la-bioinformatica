def proteina_a_rna(protein_sequence):
    #diccionario de código genético
    codigo_genetico = {
        'A': 'GCU', 'C': 'UGC', 'D': 'GAU', 'E': 'GAA',
        'F': 'UUU', 'G': 'GGU', 'H': 'CAU', 'I': 'AUU',
        'K': 'AAA', 'L': 'UUG', 'M': 'AUG', 'N': 'AAU',
        'P': 'CCU', 'Q': 'CAA', 'R': 'CGU', 'S': 'UCU',
        'T': 'ACU', 'V': 'GUU', 'W': 'UGG', 'Y': 'UAU',
        # Codones de parada
        'Stop': ['UAA', 'UGA', 'UAG']
    }

    # Convertir la secuencia de aminoácidos a ARN
    secuencia_rna = ''
    for amino_acido in protein_sequence:
        if amino_acido == 'Stop':
            break
        secuencia_rna += codigo_genetico[amino_acido]

    return secuencia_rna

# Ejemplo de secuencia de aminoácidos
secuencia_proteica = 'ATVEKGGKHKTGPNEKGKKIFVQKCSQCHTVLHGLFGRKTGQA'

# Convertir la secuencia de aminoácidos a ARN
arn_codificante = proteina_a_rna(secuencia_proteica)

# Imprimir la secuencia de ARN codificante
print('Secuencia de ARN codificante: {}'.format(arn_codificante))
