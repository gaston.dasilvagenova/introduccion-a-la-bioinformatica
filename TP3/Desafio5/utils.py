import random

def rna_a_aminoacido(arn_to_protein):
    # Tabla de codones que mapea codones de ARN a aminoácidos
    tabla_codones = {
        'GCU': 'A', 'UGC': 'C', 'GAU': 'D', 'GAA': 'E',
        'UUU': 'F', 'GGU': 'G', 'CAU': 'H', 'AUU': 'I',
        'AAA': 'K', 'UUG': 'L', 'AUG': 'M', 'AAU': 'N',
        'CCU': 'P', 'CAA': 'Q', 'CGU': 'R', 'UCU': 'S',
        'ACU': 'T', 'GUU': 'V', 'UGG': 'W', 'UAU': 'Y',
        # Codones de parada
        'UAA': 'Stop', 'UGA':'Stop', 'UAG':'Stop'
    }
    return codigo_genetico[amino_acido]

def imprimir_estadisticas_jugador(jugador):
    print("Estadísticas del jugador:")
    print("+------------------+")
    print("|    Nombre:       |", jugador.nombre)
    print("|    Salud:        |", jugador.salud)
    print("|    Experiencia:  |", jugador.experiencia)
    print("+------------------+")



def imprimir_secuencia_arn(secuencia_arn):
    print("Cadena de ARN encontrada:")
    print("+---------------------+")
    print("|    Secuencia        |")
    print("+---------------------+")
    print("|", " ".join(secuencia_arn), " "*(10- len(secuencia_arn)), "|")
    print("+---------------------+")

def imprimir_muerte_jugador(nombre_jugador):
    print("¡¡¡ TRAGEDIA EN EL CAMPO DE BATALLA !!!")
    print("El jugador ha caído en combate...")
    print("╔══════════════════════════════════════╗")
    print("║                                      ║")
    print("║          H A S   M U E R T O         ║")
    print("║                                      ║")
    print("╚══════════════════════════════════════╝")


def imprimir_proteina(proteina):
    # Diccionario que mapea códigos de aminoácidos a nombres
    nombres_aminoacidos = {
        "A": "Alanina", "R": "Arginina", "N": "Asparagina", "D": "Ácido aspártico",
        "C": "Cisteína", "Q": "Glutamina", "E": "Ácido glutámico", "G": "Glicina",
        "H": "Histidina", "I": "Isoleucina", "L": "Leucina", "K": "Lisina",
        "M": "Metionina", "F": "Fenilalanina", "P": "Prolina", "S": "Serina",
        "T": "Treonina", "W": "Triptófano", "Y": "Tirosina", "V": "Valina",
        "*": "STOP"
    }

    # Imprimir encabezado
    print("Secuencia de proteína:")
    print("+------------+--------------+")
    print("| Aminoácido | Nombre       |")
    print("+------------+--------------+")

    # Imprimir cada aminoácido con su nombre
    for aminoacido in proteina:
        nombre_aminoacido = nombres_aminoacidos.get(aminoacido, "Desconocido")
        print(f"|     {aminoacido}      | {nombre_aminoacido: <13} |")

    # Imprimir línea final
    print("+----------- +--------------+")

def secuencia_arn_aleatoria():
    # Ejemplos de cadenas de ARN
    secuencias_arn_ejemplos = [
        "GCUCCUGUU", # APV
        "UGGUAUAAA", # WYK
        "CAUAAAAAA", # HKK
    ]
    
    # Selecciona una secuencia de ARN al azar
    secuencia_arn_random = random.choice(secuencias_arn_ejemplos)
    return secuencia_arn_random

def proteina_a_rna(protein_sequence):
    #diccionario de código genético
    codigo_genetico = {
        'A': 'GCU', 'C': 'UGC', 'D': 'GAU', 'E': 'GAA',
        'F': 'UUU', 'G': 'GGU', 'H': 'CAU', 'I': 'AUU',
        'K': 'AAA', 'L': 'UUG', 'M': 'AUG', 'N': 'AAU',
        'P': 'CCU', 'Q': 'CAA', 'R': 'CGU', 'S': 'UCU',
        'T': 'ACU', 'V': 'GUU', 'W': 'UGG', 'Y': 'UAU',
        # Codones de parada
        'Stop': ['UAA', 'UGA', 'UAG']
    }

    # Convertir la secuencia de aminoácidos a ARN
    secuencia_rna = ''
    for amino_acido in protein_sequence:
        if amino_acido == 'Stop':
            break
        secuencia_rna += codigo_genetico[amino_acido]

    return secuencia_rna

def validar_transcripcion_proteica(secuencia_proteina, secuencia_arn):
    # secuencia de ARN resultante de la transcripción proteica
    secuencia_arn_transcrita = proteina_a_rna(secuencia_proteina)

    # comparacion la secuencia de ARN resultante con la secuencia de ARN dada
    if secuencia_arn_transcrita == secuencia_arn:
        return True
    else:
        return False