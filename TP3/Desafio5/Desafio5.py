import random
import sys
import utils

class Jugador:
    def __init__(self, nombre):
        self.nombre = nombre
        self.salud = 100
        self.experiencia = 0
        self.muerto_en_batalla = False

    def atacar(self):
        return random.randint(10, 20)

    def subir_salud(self,salud_a_sumar):
        nueva_salud = self.salud + salud_a_sumar
        if(nueva_salud>100):
            nueva_salud = 100
        self.salud = nueva_salud
        
    def recibir_danio(self, danio):
        self.salud -= danio

    def ganar_experiencia(self, experiencia):
        self.experiencia += experiencia

    def restar_experiencia(self, experiencia):

        exp_return = 0 
        if(experiencia <=  self.experiencia):
            exp_return = self.experiencia - experiencia
        self.experiencia = exp_return

# batalla con un enemigo
def batalla(jugador):
    salud_enemigo = random.randint(50, 100)
    print("¡Te has encontrado con un enemigo microbiano!")
    while True:
        accion = input("¿Qué quieres hacer? (atacar/huir): ").lower()
        if accion == "atacar":
            danio_jugador = jugador.atacar()
            danio_enemigo = random.randint(5, 15)
            print(f"¡Has infligido {danio_jugador} de daño al enemigo!")
            print(f"El enemigo te infligió {danio_enemigo} de daño.")
            jugador.recibir_danio(danio_enemigo)
            salud_enemigo -= danio_jugador

            if(jugador.salud <= 0):
                utils.imprimir_muerte_jugador(jugador.nombre)
                jugador.muerto_en_batalla = True
                break
            if salud_enemigo <= 0:
                print("¡Has derrotado al enemigo microbiano!")
                jugador.ganar_experiencia(50)
                break
            print(f"Salud del enemigo: {salud_enemigo}")
            print(f"Tu salud: {jugador.salud}")
        elif accion == "huir":
            print("¡Has escapado de la batalla!, sobrevivirás un día más pero ten en cuenta que la suerte se acaba.")
            print("¡Tu experiencia se ha visto disminuida.")
            jugador.restar_experiencia(12)


            break
        else:
            print("Acción no válida, intenta de nuevo.")



def transcripcion_arn(jugador):
    print("\n*** Transcripción de ADN ***")

    # Simulación de transcripción de ADN
    secuencia_arn_Ejemplo = utils.secuencia_arn_aleatoria() # Ejemplo de cadena de ARNs
    utils.imprimir_secuencia_arn(secuencia_arn_Ejemplo)
    
    # Lógica de transcripción de ADN
    print("Comienza la transcripción de ARN a Aminoacidos para generar Proteinas...")
   
    secuencia_proteica_success = False
    while(not secuencia_proteica_success):
        print("""
        Por favor, ingresa una secuencia de Aminoacidos [A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y],
        ¡RECUEDA LOS STOP (UAA, UGA y UAG)!:
        """)
        secuencia_proteica = input().upper()

        # Verificar si la secuencia de ARN coincide con la cadena de ADN
        if  utils.validar_transcripcion_proteica( secuencia_proteica,secuencia_arn_Ejemplo):
            utils.imprimir_proteina(secuencia_proteica)
            print("¡Transcripción exitosa!")
            print("Has sintetizado una proteína importante para la célula.")
            jugador.subir_salud(25)
            jugador.ganar_experiencia(15)  # Por ejemplo, añadir puntos al jugador por el éxito
            secuencia_proteica_success = True
        else:
            print("La secuencia de ARN no coincide con la cadena de ADN.")
            print("No se ha sintetizado ninguna proteína. !VUELVE A INTENTAR¡")


def explorar_nucleo(jugador):
    print("Te encuentras en el núcleo de la célula.")
    print("Encuentras un gen importante para la expresión génica.")
    respuesta = input("¿Quieres intentar activar la transcripción de este gen para sintetizar proteinas? (si/no): ").lower()
    if respuesta == "si":
        transcripcion_arn(jugador)
        print("De repente, un enemigo microbiano aparece.")
        batalla(jugador)
    else:
        print("Decides no hacer nada por ahora.")

# Función principal del juego
def jugar():
    print("Bienvenido a The Gene Trip: ")
    print("Eres un linfosito en busqueda de la felicidad, procesa los genes y sintetiza proteina para desarrollarte como el LinfoHero que eres.")
    print("OJO!, hay mucha oscuridad ahi afuera.")


    nombre_jugador = input("Ingresa tu nombre: ")
    jugador = Jugador(nombre_jugador)

    while True and not jugador.muerto_en_batalla:
        print("\n*** Explora la célula para descubrir los secretos ocultos de la expresión génica ***")
        print("1. Explorar el núcleo")
        print("2. Explorar el citoplasma")
        print("3. Salir del juego")

        opcion = input("¿Qué deseas hacer? (1/2/3): ")
        if opcion == "1":
           explorar_nucleo(jugador)
        elif opcion == "2":
            print("Te aventuras en el citoplasma de la célula.")
            print("Encuentras un virus que intenta desregular la expresión génica.")
            batalla(jugador)
            if(jugador.muerto_en_batalla):
                break
        elif opcion == "3":

            utils.imprimir_estadisticas_jugador(jugador)
            print("¡Gracias por jugar a TheGeneTrip!")
            break
        else:
            print("Opción no válida, intenta de nuevo.")

def main():
    print("""
    ╭──────────────────────────────────────────╮
    │                                          │
    │               THE GENE TRIP              │
    │                                          │
    ╰──────────────────────────────────────────╯
    
        Bienvenido a The Gene Trip,
    un emocionante viaje a través del ADN.
    
     Presiona ENTER para comenzar el juego...
     
                ▄▄▄▄▄▄▄▄▄▄▄
               █            █
              █              █
             █    █    █    █
             █    █    █    █
              █              █
               █            █
                ▀▀▀▀▀▀▀▀▀▀▀
                
    """)

    # Espera a que se presione Enter para comenzar el juego
    while True:
        key = sys.stdin.read(1)
        if key == '\n':
            jugar()
            break

if __name__ == "__main__":
    main()

