def encontrar_regiones_promotoras():
    # Lee la secuencia de ADN desde el archivo
    archivo = open('TP3/otros/secuencia_adn.txt', 'r')

    secuencia_adn = archivo.read().strip()

    # Secuencia de la caja TATA
    caja_tata = 'TATAAA'

    # Buscar las regiones promotoras en la secuencia de ADN
    cant_regiones_promotoras = 0
    inicio = 0
    print("Regiones promotoras encontradas:")
    
    while inicio < len(secuencia_adn):
        # Buscamos la próxima ocurrencia de la caja TATA
        inicio_caja_tata = secuencia_adn.find(caja_tata, inicio)
        if inicio_caja_tata == -1:
            break
        fin_caja_tata = inicio_caja_tata + len(caja_tata) - 1

        # Sumamos 1 al contador de regiones promotoras.
        cant_regiones_promotoras+=1

        # Imprimir las regiones promotoras encontradas
        print(f"Región promotora {cant_regiones_promotoras}: desde el índice {inicio_caja_tata} hasta el índice {fin_caja_tata}")

        # Pasamos la siguiente búsqueda de caja TATA
        inicio = fin_caja_tata + 1

# Encontrar las regiones promotoras en la secuencia de ADN
regiones_promotoras = encontrar_regiones_promotoras()
