import re


class SecondStrucuturePredictor:
    def __init__(self):
        # Parámetros para la predicción de estructura secundaria
        self.preferences = {
            'L': {'Glu': 1.01, 'Val': 0.41, 'Gly': 1.77, 'Ala': 0.82, 'Tyr': 0.76, 'Asn': 1.34}, # Loop
            'H': {'Glu': 1.59, 'Val': 0.90, 'Gly': 0.43, 'Ala': 1.41, 'Tyr': 0.74, 'Asn': 0.76}, # Alfa Helix
            'B': {'Glu': 0.52, 'Val': 1.87, 'Gly': 0.58, 'Ala': 0.72, 'Tyr': 1.45, 'Asn': 0.48}, # B strand
        }

    def getSplitSequence(self, sequence):
        # Utilizamos una expresión regular para dividir la cadena en cada letra mayúscula
        # excepto la primera para evitar dividir la primera letra de cada palabra.
        return re.findall('[A-Z][a-z]*', sequence)


    def predict(self, sequence):
        predicted_structure = ''

        sequenced_split = self.getSplitSequence(sequence)

        for aminoacid in sequenced_split:
            #si el aminoacido no esta configurado en preferences se lo toma como un Loop "L"
            predicted_structure += max(self.preferences.keys(), key=lambda x: self.preferences[x].get(aminoacid, 0))

        return predicted_structure


# Ejemplo de uso
second_structure_predictor = SecondStrucuturePredictor()
sequence = input("Ingresa la secuencia de aminoácidos: ")  # Solicitamos la secuencia al usuario

#"GluGlyTyrTyrValGluAsnSat"  # Secuencia proteica de ejemplo
predicted_structure = second_structure_predictor.predict(sequence)
print("Estructura secundaria predicha:", predicted_structure)
